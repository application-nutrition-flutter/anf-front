import 'dart:convert';

import 'package:api_cache_manager/api_cache_manager.dart';
import 'package:api_cache_manager/models/cache_db_model.dart';
import 'package:flutter/cupertino.dart';

class AuthService {
  static Future<bool> isLogged() async {
    return await APICacheManager().isAPICacheKeyExist("jwt_token");
  }

  static Future<String?> getToken() async {
    if (await AuthService.isLogged()) {
      var cacheData = await APICacheManager().getCacheData("jwt_token");

      return json.decode(cacheData.syncData);
    }
    return null;
  }

  static Future<void> setToken(String token) async {
    APICacheDBModel cacheDBModel =
        APICacheDBModel(key: "jwt_token", syncData: token);

    await APICacheManager().addCacheData(cacheDBModel);
  }

  static Future<void> logout(BuildContext context) async {
    await APICacheManager().deleteCache("jwt_token");
    Navigator.pushNamedAndRemoveUntil(context, "/login", (route) => false);
  }
}
