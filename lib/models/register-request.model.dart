class RegisterRequestModel {
  RegisterRequestModel({
    required this.email,
    required this.password,
    required this.confirmPassword,
  });
  late final String email;
  late final String password;
  late final String confirmPassword;

  RegisterRequestModel.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    password = json['password'];
    confirmPassword = json['confirmPassword'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['email'] = email;
    _data['password'] = password;
    _data['confirmPassword'] = confirmPassword;
    return _data;
  }
}
