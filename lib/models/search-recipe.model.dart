class SearchRecipe {

  final List<SearchedRecipe>? searchRecipe;

  SearchRecipe({this.searchRecipe});

    factory SearchRecipe.fromJson(Map<String, dynamic> parsedJson) {
    var list = parsedJson['results'] as List;
    //print(list.runtimeType);

    List<SearchedRecipe> recipeList = list.map((i) => SearchedRecipe.fromJson(i)).toList();
    return SearchRecipe(searchRecipe: recipeList);
  }
}

class SearchedRecipe {

  int? id;
  String? title;
  int? readyInMinutes;
  String? sourceUrl;
  String? image;

  SearchedRecipe({
    this.id,
    this.title,
    this.readyInMinutes,
    this.sourceUrl,
    this.image,
  });

  factory SearchedRecipe.fromJson(Map<String, dynamic> json) {
    return SearchedRecipe(
      id: json["id"] as int,
      title: json["title"] as String,
      readyInMinutes: json["readyInMinutes"] as int,
      sourceUrl: json["sourceUrl"] as String,
      image: json["image"] as String,
    );
  }
}
