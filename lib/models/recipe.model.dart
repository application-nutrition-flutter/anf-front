class RandomRecipe {
  final List<Recipe>? recipes;
  // final List<ExtendedIngredients>? ingredient;

  RandomRecipe({this.recipes});

  factory RandomRecipe.fromJson(Map<String, dynamic> parsedJson) {
    var list = parsedJson['recipes'] as List;
    //print(list.runtimeType);

    List<Recipe> recipeList = list.map((i) => Recipe.fromJson(i)).toList();
    return RandomRecipe(recipes: recipeList);
  }

  //factory RandomRecipe.fromJson(Map<String, dynamic> parsedJson) {
  //  var listRecipe = parsedJson['recipes'] as List;
  //  print(listRecipe.runtimeType);
  //  List<Recipe> recipeList =
  //      listRecipe.map((i) => Recipe.fromJson(i)).toList();

  //var listIngredient = parsedJson['ingredient'] as List;
  //print(listIngredient.runtimeType);
  //List<ExtendedIngredients> ingredientList =
  //    listIngredient.map((i) => ExtendedIngredients.fromJson(i)).toList();

  // return RandomRecipe(recipes: recipeList);
}
//}

class Recipe {
  bool? vegetarian;
  bool? vegan;
  bool? glutenFree;
  bool? dairyFree;
  bool? veryHealthy;
  bool? cheap;
  bool? veryPopular;
  bool? sustainable;
  int? weightWatcherSmartPoints;
  String? gaps;
  bool? lowFodmap;
  bool? ketogenic;
  bool? whole30;
  int? servings;
  int? preparationMinutes;
  int? cookingMinutes;
  String? sourceUrl;
  String? spoonacularSourceUrl;
  int? aggregateLikes;
  String? creditsText;
  String? sourceName;
  int? id;
  String? title;
  int? readyInMinutes;
  String? image;
  String? imageType;
  String? instructions;

  Recipe(
      {this.vegetarian,
      this.vegan,
      this.glutenFree,
      this.dairyFree,
      this.veryHealthy,
      this.cheap,
      this.veryPopular,
      this.sustainable,
      this.weightWatcherSmartPoints,
      this.gaps,
      this.lowFodmap,
      this.ketogenic,
      this.whole30,
      this.servings,
      this.preparationMinutes,
      this.cookingMinutes,
      this.sourceUrl,
      this.spoonacularSourceUrl,
      this.aggregateLikes,
      this.creditsText,
      this.sourceName,
      this.id,
      this.image,
      this.imageType,
      this.instructions,
      this.readyInMinutes,
      this.title});

  factory Recipe.fromJson(Map<String, dynamic> parsedJson) {
    return Recipe(
      vegetarian: parsedJson['vegetarian'],
      vegan: parsedJson['vegan'],
      glutenFree: parsedJson['glutenFree'],
      dairyFree: parsedJson['dairyFree'],
      veryHealthy: parsedJson['veryHealthy'],
      cheap: parsedJson['cheap'],
      veryPopular: parsedJson['veryPopular'],
      sustainable: parsedJson['sustainable'],
      weightWatcherSmartPoints: parsedJson['weightWatcherSmartPoints'],
      gaps: parsedJson['gaps'],
      lowFodmap: parsedJson['lowFodmap'],
      ketogenic: parsedJson['ketogenic'],
      whole30: parsedJson['whole30'],
      servings: parsedJson['servings'],
      preparationMinutes: parsedJson['preparationMinutes'],
      cookingMinutes: parsedJson['cookingMinutes'],
      sourceUrl: parsedJson['sourceUrl'],
      spoonacularSourceUrl: parsedJson['spoonacularSourceUrl'],
      aggregateLikes: parsedJson['aggregateLikes'],
      creditsText: parsedJson['creditsText'],
      sourceName: parsedJson['sourceName'],
      id: parsedJson['id'],
      title: parsedJson['title'],
      readyInMinutes: parsedJson['readyInMinutes'],
      image: parsedJson['image'],
      imageType: parsedJson['imageType'],
      instructions: parsedJson['instructions'],
    );
  }
}