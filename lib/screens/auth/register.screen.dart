import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:flutter_project/api/users.api.dart';
import 'package:flutter_project/models/register-request.model.dart';
import 'package:hexcolor/hexcolor.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  bool hidePassword = true;
  bool hideConfirmPassword = true;
  final _formKey = GlobalKey<FormState>();
  String? email = "";
  String? password = "";
  String? confirmPassword = "";

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: HexColor("#FFFFFF"),
        body: ProgressHUD(
          child: Form(key: _formKey, child: _registerUI(context)),
        ),
      ),
    );
  }

  Widget _registerUI(BuildContext context) {
    return SingleChildScrollView(
      child: Column(children: [
        Column(
          children: [
            Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 80.0),
                  child: Column(
                    children: const [
                      Padding(
                        padding: EdgeInsets.only(bottom: 16.0),
                        child: Text(
                          "Inscription",
                          style: TextStyle(
                            fontSize: 28.0,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.symmetric(horizontal: 30.0),
                          child: Text(
                            "Inscrivez-vous dès maintenant et trouvez la recette qui vous correspond !",
                            style: TextStyle(fontSize: 14.0),
                            textAlign: TextAlign.center,
                          ))
                    ],
                  ),
                )),
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Column(children: [
                    TextFormField(
                      initialValue: email,
                      onChanged: (val) {
                        setState(() {
                          email = val;
                        });
                      },
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                          labelText: 'Adresse mail',
                          labelStyle: TextStyle(color: HexColor("#30312F")),
                          fillColor: HexColor("#F1F1F1"),
                          filled: true,
                          hintText: "Saisissez votre adresse mail",
                          border: const OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                            borderSide: BorderSide.none,
                          )),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "L'email est obligatoire";
                        }
                        return null;
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 30.0),
                      child: TextFormField(
                        initialValue: password,
                        onChanged: (val) {
                          setState(() {
                            password = val;
                          });
                        },
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: hidePassword,
                        decoration: InputDecoration(
                            suffixIcon: IconButton(
                                onPressed: () {
                                  setState(() {
                                    hidePassword = !hidePassword;
                                  });
                                },
                                icon: Icon(
                                    hidePassword
                                        ? Icons.remove_red_eye
                                        : Icons.remove_red_eye_outlined,
                                    color: HexColor("#30312F"),
                                    size: 15)),
                            labelText: 'Mot de passe',
                            labelStyle: TextStyle(color: HexColor("#30312F")),
                            fillColor: HexColor("#F1F1F1"),
                            filled: true,
                            hintText: "Saisissez votre mot de passe",
                            border: const OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(10.0),
                              ),
                              borderSide: BorderSide.none,
                            )),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Le mot de passe est obligatoire";
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 30.0),
                      child: TextFormField(
                        initialValue: confirmPassword,
                        onChanged: (val) {
                          setState(() {
                            confirmPassword = val;
                          });
                        },
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: hideConfirmPassword,
                        decoration: InputDecoration(
                            suffixIcon: IconButton(
                                onPressed: () {
                                  setState(() {
                                    hideConfirmPassword = !hideConfirmPassword;
                                  });
                                },
                                icon: Icon(
                                    hideConfirmPassword
                                        ? Icons.remove_red_eye
                                        : Icons.remove_red_eye_outlined,
                                    color: HexColor("#30312F"),
                                    size: 15)),
                            labelText: 'Confirmation',
                            labelStyle: TextStyle(color: HexColor("#30312F")),
                            fillColor: HexColor("#F1F1F1"),
                            filled: true,
                            hintText: "Saisissez de nouveau votre mot de passe",
                            border: const OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(10.0),
                              ),
                              borderSide: BorderSide.none,
                            )),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "La confirmation est obligatoire";
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 40.0),
                      child: TextButton(
                        onPressed: () {
                          if (validateAndSave()) {
                            RegisterRequestModel model = RegisterRequestModel(
                                email: email!,
                                password: password!,
                                confirmPassword: confirmPassword!);

                            UsersAPI.register(model).then((response) => {
                                  if (response.status == "success")
                                    {
                                      Navigator.pushNamedAndRemoveUntil(
                                          context, "/home", (route) => false)
                                    }
                                  else
                                    {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(SnackBar(
                                              content: Text(
                                                  response.data.toString())))
                                    }
                                });
                          }
                        },
                        style: ButtonStyle(
                            minimumSize: MaterialStateProperty.all<Size>(
                                const Size.fromHeight(50)),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            )),
                            padding: MaterialStateProperty.all<EdgeInsets>(
                                const EdgeInsets.symmetric(
                                    vertical: 15.0, horizontal: 20.0)),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                HexColor("#63B102"))),
                        child: Text(
                          "S'inscrire",
                          style: TextStyle(color: HexColor("#FFFFFF")),
                        ),
                      ),
                    ),
                  ]),
                ),
              ],
            ),
          ],
        ),
        Container(
            padding: const EdgeInsets.symmetric(vertical: 60.0),
            child: RichText(
                text: TextSpan(children: [
              const TextSpan(text: "Pas encore connecté ? "),
              TextSpan(
                  text: "C’est par ici !",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      color: HexColor("#63B102")),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      Navigator.pushNamed(context, "/");
                    })
            ], style: TextStyle(color: HexColor("#30312F")))))
      ]),
    );
  }

  bool validateAndSave() {
    final form = _formKey.currentState;

    if (form!.validate()) {
      form.save();
      return true;
    }

    return false;
  }
}
