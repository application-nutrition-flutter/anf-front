import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_project/api/recipe.api.dart';
import 'package:flutter_project/models/search-recipe.model.dart';
import 'package:google_fonts/google_fonts.dart';

class SearchRecipes extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Recipes',
              style: GoogleFonts.nunito(
                  color: Colors.black,
                  fontSize: 22,
                  fontWeight: FontWeight.w800)),
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(Icons.arrow_back, color: Colors.grey[800], size: 20),
          ),
          actions: [
            IconButton(
              icon: Icon(Icons.favorite_outline_rounded,
                  color: Colors.grey[700], size: 20),
              onPressed: () {},
            ),
            IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.search,
                  color: Colors.grey[700],
                  size: 20,
                ))
          ],
          centerTitle: true,
        ),
        body: Center(
          child: FutureBuilder<SearchRecipe>(
            future: getSearchRecipe(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                SearchRecipe? recipe = snapshot.data;

                return ListView.builder(
                    itemCount: recipe?.searchRecipe?.length ?? 0,
                    itemBuilder: (BuildContext context, int index) {
                      final aReceipe = recipe!.searchRecipe![index];

                      return Container(
                          padding: const EdgeInsets.fromLTRB(10, 25, 10, 10),
                          color: Colors.white,
                          child: Column(
                            children: [
                              Text(
                                aReceipe.title ?? "",
                                style: GoogleFonts.nunito(
                                    color: Colors.black,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w800),
                                textAlign: TextAlign.center,
                              ),
                              const SizedBox(height: 20),
                              Text(
                                "Preparation : " + (aReceipe.readyInMinutes).toString() + " min",
                                style: const TextStyle(fontSize: 14.0),
                                textAlign: TextAlign.center,
                              ),
                              const SizedBox(height: 20),
                              // Image.network(aReceipe.image ?? "")
                            ],
                          ));
                    });
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}',
                    style: const TextStyle(fontSize: 30.0));
              }
              return const CircularProgressIndicator();
            },
          ),
        ));
  }
}
