import 'package:flutter/material.dart';

class NutritionResearch extends StatefulWidget {

  @override

  _NutritionResearchState createState() => _NutritionResearchState();
}

class _NutritionResearchState extends State<NutritionResearch> {

  @override

  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(10, 25, 10, 10),
      child: Row(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(30),
                  boxShadow: const [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius:  4,
                      offset: Offset(0,3),
                    ),
                  ]
                ),
                child: const TextField(
                  decoration: InputDecoration(
                  hintText: 'Search Recipes',
                  contentPadding: EdgeInsets.all(10),
                  border: InputBorder.none,
                )
                ),
            )
          ),
          const SizedBox(width: 10),
          Container(
                height: 50,
                width: 50,
                decoration: const BoxDecoration(
                  
                  boxShadow: [
                    BoxShadow(
                    color: Colors.green,
                    blurRadius: 4,
                    offset: Offset(0,4),
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(25),
                  ),
                ),
                child: ElevatedButton(
                  onPressed: () {},
                  child: const Icon(
                    Icons.search,
                    size: 26,
                  ),
                  style: ElevatedButton.styleFrom(
                    shape: const CircleBorder(),
                    padding: const EdgeInsets.all(10),
                    primary: Colors.green
                   
                  )
                )
              )
        ],
      )

    );
  }
}