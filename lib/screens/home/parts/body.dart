//import 'package:flutter/material.dart';
//import 'package:flutter_project/api/recipe.api.dart';
//import 'package:flutter_project/models/recipe.model.dart';
//import 'package:flutter_project/screens/home/parts/search_recipes.dart';
//import 'package:google_fonts/google_fonts.dart';
//
//class Body extends StatelessWidget {
//  const Body({Key? key}) : super(key: key);
//
//  @override
//  Widget build(BuildContext context) {
//    return Center(
//          child: FutureBuilder<RandomRecipe>(
//            future: getRandomRecipe(),
//            builder: (context, snapshot) {
//              if (snapshot.hasData) {
//                RandomRecipe? recipe = snapshot.data;
//
//                return ListView.builder(
//                    itemCount: recipe?.recipes?.length ?? 0,
//                    itemBuilder: (BuildContext context, int index) {
//                      final aReceipe = recipe!.recipes![index];
//
//                      return Container(
//                          padding: const EdgeInsets.fromLTRB(10, 25, 10, 10),
//                          color: Colors.white,
//                          child: Column(
//                            children: [
//                              Text(
//                                aReceipe.title ?? "",
//                                style: GoogleFonts.nunito(
//                                    color: Colors.black,
//                                    fontSize: 20,
//                                    fontWeight: FontWeight.w800),
//                                textAlign: TextAlign.center,
//                              ),
//                              const SizedBox(height: 20),
//                              Text(
//                                aReceipe.instructions ?? "",
//                                style: const TextStyle(fontSize: 14.0),
//                                textAlign: TextAlign.center,
//                              ),
//                              const SizedBox(height: 20),
//                              Image.network(aReceipe.image ?? "")
//                            ],
//                          ));
//                    });
//              } else if (snapshot.hasError) {
//                return Text('${snapshot.error}',
//                    style: const TextStyle(fontSize: 30.0));
//              }
//              return const CircularProgressIndicator();
//            },
//          ),
//        );
//  }
//}
