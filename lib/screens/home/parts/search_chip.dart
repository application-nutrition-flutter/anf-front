import 'package:flutter/material.dart';

class SearchChip extends StatefulWidget {
  const SearchChip({Key? key}) : super(key: key);
  @override

  _SearchChipState createState() => _SearchChipState();
}

class Tech {
  String label;
  Color color;
  bool isSelected;
  Tech(this.label, this.color, this.isSelected);
}

class _SearchChipState extends State<SearchChip> {

  bool selected = false;

  final List<Tech> _chipList = [
    Tech("Hamburger", Colors.red, false),
    Tech("Pizza", Colors.yellow, false),
    Tech("Tacos", Colors.amber, false),
  ];

  @override

  Widget build(BuildContext context) {
    return Container(
        //padding: const EdgeInsets.fromLTRB(10, 25, 10, 10),
        //color: Colors.white,
        child: Wrap(
          spacing: 8,
          direction: Axis.horizontal,
          children: techChips()
        ));
  }

  List<Widget> techChips () {
    List<Widget> chips = [];
    for(int i=0; i<_chipList.length; i++) {
      Widget item = Padding(
        padding: const EdgeInsets.only(left: 10, right: 5),
        child: FilterChip(
          label: Text(_chipList[i].label),
          labelStyle: const TextStyle(color: Colors.white),
          backgroundColor: _chipList[i].color,
          selected: _chipList[i].isSelected,
          onSelected: (bool value) {
            setState(() {
              _chipList[i].isSelected = value;
            });
          },
        ),
      );
      chips.add(item);
    }
    return chips;
  }
}




