import 'package:flutter/material.dart';
import 'package:flutter_project/screens/home/parts/search_recipes.dart';

class NutritionBar extends StatelessWidget implements PreferredSizeWidget {
  @override

  Size get preferredSize => const Size.fromHeight(50);

  Widget build(BuildContext context) {
    return AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: null,
            icon: Icon(Icons.fastfood, color: Colors.green[600], size: 20),
          ),
          actions: [
            IconButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return SearchRecipes();
                  }));
                },
                icon: Icon(
                  Icons.account_circle,
                  color: Colors.grey[700],
                  size: 20,
                ))
          ],
          centerTitle: true,
    );
  }
}