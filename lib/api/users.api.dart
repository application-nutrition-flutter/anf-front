import 'dart:convert';
import 'package:flutter_project/config.dart';
import 'package:flutter_project/models/login-request.model.dart';
import 'package:flutter_project/models/login-response.model.dart';
import 'package:flutter_project/models/register-request.model.dart';
import 'package:flutter_project/services/auth.service.dart';
import 'package:http/http.dart' as http;

const String apiUrl = Config.apiUrl;
const String baseUrl = "$apiUrl/users";

class UsersAPI {
  static var client = http.Client();

  static Future<LoginResponseModel> login(LoginRequestModel model) async {
    Map<String, String> requestHeaders = {
      "Content-Type": "application/json",
    };

    var url = Uri.parse('$baseUrl/login');
    var response = await client.post(url,
        headers: requestHeaders, body: jsonEncode(model.toJson()));
    var jsonResponse = loginResponseJson(response.body);

    if (jsonResponse.status == "success") {
      await AuthService.setToken(jsonResponse.data);
    }

    return jsonResponse;
  }

  static Future<LoginResponseModel> register(RegisterRequestModel model) async {
    Map<String, String> requestHeaders = {
      "Content-Type": "application/json",
    };

    var url = Uri.parse('$baseUrl/register');
    var response = await client.post(url,
        headers: requestHeaders, body: json.encode(model.toJson()));
    var jsonResponse = loginResponseJson(response.body);

    if (jsonResponse.status == "success") {
      await AuthService.setToken(jsonResponse.data);
    }

    return jsonResponse;
  }
}
