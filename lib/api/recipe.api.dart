import 'dart:convert';
import 'package:flutter_project/models/recipe.model.dart';
import 'package:flutter_project/models/search-recipe.model.dart';
// import 'package:flutter_project/screens/home/parts/search_recipes.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_dotenv/flutter_dotenv.dart';

Future<RandomRecipe> getRandomRecipe() async {
  final response = await http.get(
      Uri.parse(
          "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/random"),
      headers: {'x-rapidapi-key': dotenv.env['API_KEY']!});

  if (response.statusCode == 200) {
    Map<String, dynamic> map = json.decode(response.body);

    final recipes = RandomRecipe.fromJson(map);
    return recipes;
  } else {
    throw Exception("Pas de données disponibles");
  }
}

Future<SearchRecipe> getSearchRecipe() async {
  final response = await http.get(Uri.parse(
      "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/search"),
      headers: {'x-rapidapi-key' : dotenv.env['API_KEY']!});

  if (response.statusCode == 200) {
    print(response.body);
    // List jsonResponse = json.decode(response.body);
    Map <String, dynamic> map = json.decode(response.body);

    final recipes = SearchRecipe.fromJson(map);
    return recipes;
  } else {
    throw Exception("Pas de données disponibles");
  }
}
