import 'package:flutter/material.dart';
import 'package:flutter_project/screens/auth/login.screen.dart';
import 'package:flutter_project/screens/auth/register.screen.dart';
import 'package:flutter_project/screens/home/home.screen.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_project/screens/home/parts/nutrition.dart';
import 'package:flutter_project/screens/home/parts/search_chip.dart';
import 'package:flutter_project/screens/home/parts/text_field_research.dart';
import 'package:flutter_project/services/auth.service.dart';

Widget _defaultHome = const LoginScreen();

Future main() async {
  await dotenv.load(fileName: ".env");

  if (await AuthService.isLogged()) {
    _defaultHome = const HomePage();
  }

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Project',
      theme: ThemeData(),
      routes: {
        "/": (context) => _defaultHome,
        "/register": (context) => const RegisterScreen(),
        "/home": (context) => const HomePage()
      },
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: NutritionBar(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              NutritionResearch(),
              const SearchChip(),
              const HomeScreen(),
            ],
          ),
        ));
  }
}
