# Application Nutrition Flutter - Front mobile application

Project for the ydays 💻

Here is the [link to the wireframes](https://www.figma.com/proto/JeBJcTgVLYPqIs7zTXnhmM/ANF?node-id=1%3A5) (created on Figma)

## Installation

Install the repository by cloning it

```sh
git clone https://gitlab.com/application-nutrition-flutter/anf-front.git
```

Get packages in a Flutter project

```sh
flutter pub get
```

## Usage

This project is using Jira in order to work with a scrum workflow

### For Git

For naming the branch (according Jira) : **\<projectKey\>-\<ticketCode\>**

> ANF-4

For naming the commit : **\<gitmoji\> \<message\>**

> 🎉 Init project

## Development Team

- [Mickael BRUNET](https://gitlab.com/mickael.brunet64) - Bachelor3 Expert in Web Development
- [Quentin MASBERNAT](https://gitlab.com/quentin.masbernat) - Master1 Expert in Web Development
